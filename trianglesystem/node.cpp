#include "node.h"
#include <array>


Node::Node(GMlib::TSVertex<float> *i)
{
    _v = i;
}

Node::Node()
{
}

GMlib::Array<GMlib::TSTriangle<float> *> Node::getTriangles()
{

    return _v->getTriangles();

}

GMlib::TSEdge<float> *Node::getNeighbor(Node &n)
{
    // get current vertex cordinates
    GMlib::Array<GMlib::TSEdge<float>*> edge = _v->getEdges();
    for (int i = 0; i < edge.size(); ++i) {
        if(n.equal(edge[i]->getOtherVertex(*_v)))
            return edge[i];
    }
    return NULL;
}

bool Node::equal(GMlib::TSVertex<float> *v)
{
    if ( v == _v)
        return true;
    return false;
}

void Node::setZ(float z)
{
    _v->setZ(z);
}
