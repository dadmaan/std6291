#ifndef NODE_H
#define NODE_H

#include "../modules/trianglesystem/gmtrianglesystem.h"

class Node{


public:
    // data member
    GMlib::TSVertex<float>* _v;
    //constructors

    Node(GMlib::TSVertex<float> *i);
    Node();
    // getters
    GMlib::Array<GMlib::TSTriangle<float> *> getTriangles();
    GMlib::TSEdge<float> *getNeighbor(Node &n);
    bool equal(GMlib::TSVertex<float> *v);

    // setters
    void setZ(float z);
};// namespace node

#endif // NODE_H
