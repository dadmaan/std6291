#ifndef SCENARIO_H
#define SCENARIO_H



#include "application/gmlibwrapper.h"

// qt
#include <QObject>

namespace GMlib {
  template <typename T> class TriangleFacets;

}
class FEMObject;

class Scenario : public GMlibWrapper {
  Q_OBJECT
public:
  using GMlibWrapper::GMlibWrapper;

  void    initializeScenario() override;
  void    cleanupScenario() override;
  void    animateMembrane();

private:

    // Triangle Facet
    void initializeTriangleFacetExample();
    void cleanupTriangleFacetExample();

    // Torus
    void initializeTestTorusExample();
    void cleanupTestTorusExample();

    // Regular Triangulation
    void initializeRegularMembraneExample();
    void cleanupRegularMembraneExample();

    // Random Triangulation
    void initializeRandomMembraneExample();
    void cleanupRandomMembraneExample();

    std::shared_ptr<TestTorus>                    m_test_torus{nullptr};
    std::shared_ptr<GMlib::TriangleFacets<float>> m_triangle_facets{nullptr};
    std::shared_ptr<FEMObject> m_regular_membrane{nullptr};
    std::shared_ptr<FEMObject> m_random_membrane{nullptr};
};

#endif // SCENARIO_H
