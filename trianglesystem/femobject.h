#ifndef FEMOBJECT_H
#define FEMOBJECT_H

#include "node.h"
#include "../modules/trianglesystem/gmtrianglesystem.h"
#include "../modules/core/types/gmmatrix.h"
#include "../modules/core/containers/gmarrayt.h"

class FEMObject : public GMlib::TriangleFacets<float> {


    // data members
    GMlib::ArrayLX<Node> _nodes;
    GMlib::DMatrix<float> _A;
    GMlib::DVector<float> _b;

    float   _f;
    float   max_f;
    bool force_up;
    bool force_down;
    // utilities
public:

    FEMObject(){
        _f = 0;
        force_up = true;
        force_down = false;
    }
    ~FEMObject() override{

    }
    void regularTriangulation(int n, int m, float r);
    void randomTriangulation(int n, float r);
    void computation();
    // non-diagonal
    GMlib::Vector<GMlib::Vector<float,2>,3> vectorsArray(GMlib::TSEdge<float> *edge);
    // diagonal
    GMlib::Vector<GMlib::Vector<float,2>,3> vectorsArray(GMlib::TSTriangle<float> *tr, Node* n);

    void updateHeight(float f);
    void setMaxForce(float f);

    void localSimulate(double dt) override{
        if(force_up)
        {
            _f += dt;
            if(_f > max_f)
            {
                force_up =false;
                force_down = true;
            }
        }
        if (force_down)
        {
            _f -= dt;
            if(_f < -max_f)
            {
                force_up = true;
                force_down = false;
            }
        }

        updateHeight(_f);
    }

}; // namespace femobject

#endif // FEMOBJECT_H
