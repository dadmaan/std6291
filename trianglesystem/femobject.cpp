#include "femobject.h"

#include <cmath>
#include <cstdlib>
#include <stdlib.h>
#include <time.h>

void FEMObject::updateHeight(float f) {

  GMlib::DVector<float> X = _A * f * _b;

  for (int i = 0; i < _nodes.size(); i++) {
    _nodes[i].Node::setZ(X[i]);
  }
}

void FEMObject::setMaxForce(float f)
{
    max_f = f;
}

void FEMObject::regularTriangulation(int n, int m, float r) {

  this->insertAlways(GMlib::TSVertex<float>(GMlib::Point<float, 2>(0.0, 0.0)));

  for (int i = 1; i < m ; i++) {
    for (int j = 0; j < n ; j++) {
      const float alpha = (j * M_2PI)/(n);
      GMlib::SqMatrix<float, 2> R(alpha, GMlib::Vector<float, 2>(1, 0),
                                  GMlib::Vector<float, 2>(0, 1));
      GMlib::Point<float, 2> P = R * GMlib::Vector<float, 2>(i * r / m, 0);
      this->insertAlways(GMlib::TSVertex<float>(P));
    }
    n*=2;
  }
}

void FEMObject::randomTriangulation(int n, float r) {

    auto rn = 4;
    auto m = n/rn;
    regularTriangulation(rn,m,r);
    auto nm = std::max(((M_PI / (std::sqrt(3)*sin(M_PI/n)*sin(M_PI/n))+ 2 -n ) / n *0.5), (1.1));
    int num = 1 + n *nm;
    auto epsilon = 1e-5;



    //initialize random seed
     srand (time(NULL));

    for (int i=0;i<1000;i++){
          std::swap((*this)[rand() % this->size()],(*this)[rand() % this->size()]);

    }

    for (int i=num;i<this->size();i++){
        auto boundaryVertex = this->getVertex(i)->getParameter();
        auto distanceFromCenter = std::sqrt ((boundaryVertex[0]* boundaryVertex[0]) +
                                              (boundaryVertex[1] * boundaryVertex[1]) );
        if( std::abs(distanceFromCenter - r) > epsilon)
            this->removeIndex(i);
    }

}

void FEMObject::computation() {

    this->triangulateDelaunay();

  for (int i = 0; i < this->size(); i++) {
    if (!(this->getVertex(i)->boundary())) {
        this->_nodes +=Node(this->getVertex(i));
    }
  }

  // define dimension
  _A.setDim(_nodes.size(),_nodes.size());
  _b.setDim(_nodes.size());

  // compute _A and _b
  for (int i = 0; i < _nodes.size(); i++) {

    // compute non-diagnol element of the stiffness matrix
    for (int j = 0; j < i; j++) {
      GMlib::TSEdge<float>* edge = _nodes[i].getNeighbor(_nodes[j]);
      if (edge) {
          GMlib::Vector<GMlib::Vector<float,2>,3> d = vectorsArray(edge);

          const auto d0 = d[0];
          const auto a1 = d[1];
          const auto a2 = d[2];

          const auto dd = 1/std::abs(d0 * d0);
          const auto dh1 = dd * (a1 * d0);
          const auto dh2 = dd * (a2 * d0);;
          const auto area1 = std::abs(d0 ^ a1);
          const auto area2 =  std::abs(d0 ^ a2);;
          const auto h1 = dd * area1 * area1;
          const auto h2 = dd * area2 * area2;


          _A[i][j] = _A[j][i]
                   = (dh1 * (1 - dh1)/h1 - dd) * area1 /2
                   + (dh2 * (1 - dh2)/h2 - dd) * area2 /2;

      }
      else {
          _A[j][i] = _A[i][j] = 0;
      }
    }


  // insert triangles
   GMlib::Array<GMlib::TSTriangle<float> *>  triangles = _nodes[i].getTriangles();

    float TK = 0.0f;
    float sum = 0.0f;

  // compute diagonal element of stiffness matrix
  for (int t = 0; t < triangles.size(); t++) {
    GMlib::Vector<GMlib::Vector<float,2>,3> v = vectorsArray(triangles[t], &_nodes[i]);

    const auto d0 = v[0];
    const auto d1 = v[1];
    const auto d2 = v[2];

    TK += d2*d2/(std::abs(d0^d1)*2);

    // compute the load vector
    sum += triangles[t]->getArea2D()/3.0f;
  }

  _A[i][i] = TK;
  _b[i] =  sum;
  }
  _A.invert();
}

GMlib::Vector<GMlib::Vector<float,2>,3> FEMObject::vectorsArray(GMlib::TSEdge<float>* edge){

    GMlib::Vector<GMlib::Vector<float,2>,3> d;
    GMlib::Array<GMlib::TSTriangle<float>*> tr = edge->getTriangle();
    // first triangle
    GMlib::Array<GMlib::TSVertex<float>*> v1= tr[0]->getVertices();
    // second triangle
    GMlib::Array<GMlib::TSVertex<float>*> v2= tr[1]->getVertices();
    GMlib::Point<float,2> p0,p1,p2,p3;

    // our vertices
    p0 = edge->getFirstVertex()->getParameter();
    p1 = edge->getLastVertex()->getParameter();

    // find p2
    for ( int i=0 ; i<3 ; i++) {
        if(v1[i] != edge->getFirstVertex() && v1[i] != edge->getLastVertex()){
            p2 = v1[i]->getParameter();
        }
    }
    // find p3
    for ( int i=0 ; i<3 ; i++) {
        if(v2[i] != edge->getFirstVertex() && v2[i] != edge->getLastVertex()){
            p3 = v2[i]->getParameter();
        }
    }

    d[0] = p1 - p0;
    d[1] = p2 - p0;
    d[2] = p3 - p0;

    return d;
}
GMlib::Vector<GMlib::Vector<float,2>,3> FEMObject::vectorsArray(GMlib::TSTriangle<float>* tr, Node* n){

    GMlib::Point<float,2> p0,p1,p2;
    GMlib::Array<GMlib::TSVertex<float>*> v = tr->getVertices();
    GMlib::Vector<GMlib::Vector<float,2>,3> d;

    if(n->equal(v[1])){
        std::swap(v[0],v[1]);
    }
    if(n->equal(v[2])){
        std::swap(v[0],v[2]);
    }

    p0 = v[0]->getParameter();
    p1 = v[1]->getParameter();
    p2 = v[2]->getParameter();

    d[0] = p1 - p0;
    d[1] = p2 - p0;
    d[2] = p2 - p1;

    return d;
}



